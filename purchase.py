# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateAction
from trytond.model import fields
from trytond.pyson import Eval
from trytond.i18n import gettext
from trytond.model import ModelView, Workflow, fields
from trytond.exceptions import UserError, UserWarning
import logging
try:
    from jinja2 import Template as Jinja2Template
    jinja2_loaded = True
except ImportError:
    jinja2_loaded = False
    logging.getLogger('electrans').error(
        'Unable to import jinja2. Install jinja2 package.')


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    report_notes = fields.Function(fields.Text('Report notes'), 'get_report_notes')

    def get_report_notes(self, name):
        """
        Get comment from PurchaseConfiguration
        """
        PurchaseConfiguration = Pool().get('purchase.configuration')
        purchase_configuration = PurchaseConfiguration(1)
        return purchase_configuration.comment


class PurchaseConfiguration(metaclass=PoolMeta):
    __name__ = 'purchase.configuration'

    comment = fields.Text("Comment", translate=True)
